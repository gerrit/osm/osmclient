#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

import click
from osmclient.cli_commands import common
from osmclient.common import print_output
import logging

logger = logging.getLogger("osmclient")


@click.command(
    name="resource-profile-create", short_help="creates an Resource Profile to OSM"
)
@click.argument("name")
@click.option("--description", default=None, help="human readable description")
@click.pass_context
def resource_profile_create(ctx, name, description, **kwargs):
    """creates an Resource Profile to OSM

    NAME: name of the Resource Profile
    """
    logger.debug("")
    kwargs = {k: v for k, v in kwargs.items() if v is not None}
    resource_profile = kwargs
    resource_profile["name"] = name
    if description:
        resource_profile["description"] = description
    ctx.obj.resource_profile.create(name, content_dict=resource_profile)


@click.command(name="resource-profile-delete", short_help="deletes an Resource Profile")
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def resource_profile_delete(ctx, name, force):
    """deletes an Resource Profile

    NAME: name or ID of the Resource Profile to be deleted
    """
    logger.debug("")
    ctx.obj.resource_profile.delete(name, force=force)


@click.command(name="resource-profile-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@print_output.output_option
@click.pass_context
def resource_profile_list(ctx, filter, output):
    """list all Resource Profiles"""
    logger.debug("")
    common.generic_list(
        callback=ctx.obj.resource_profile.list, filter=filter, format=output
    )


@click.command(
    name="resource-profile-show",
    short_help="shows the details of an Resource Profile",
)
@click.argument("name")
@print_output.output_option
@click.pass_context
def resource_profile_show(ctx, name, output):
    """shows the details of an Resource Profile

    NAME: name or ID of the Resource Profile
    """
    logger.debug("")
    common.generic_show(callback=ctx.obj.resource_profile.get, name=name, format=output)


@click.command(name="resource-profile-update", short_help="updates an Resource Profile")
@click.argument("name")
@click.option("--newname", help="New name for the Resource Profile")
@click.option("--description", help="human readable description")
@click.pass_context
def resource_profile_update(ctx, name, newname, description, **kwargs):
    """updates an Resource Profile

    NAME: name or ID of the Resource Profile
    """
    logger.debug("")
    profile_changes = common.generic_update(newname, description, kwargs)
    ctx.obj.resource_profile.update(name, changes_dict=profile_changes)
