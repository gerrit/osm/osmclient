#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

import click
from osmclient.cli_commands import common
from osmclient.common import print_output
import logging

logger = logging.getLogger("osmclient")


@click.command(name="app-profile-create", short_help="creates an App Profile")
@click.argument("name")
@click.option("--description", default="", help="human readable description")
@click.pass_context
def app_profile_create(ctx, name, description, **kwargs):
    """creates an App Profile

    NAME: name of the App Profile
    """
    logger.debug("")
    kwargs = {k: v for k, v in kwargs.items() if v is not None}
    app_profile = kwargs
    app_profile["name"] = name
    if description:
        app_profile["description"] = description
    ctx.obj.app_profile.create(name, content_dict=app_profile)


@click.command(name="app-profile-delete", short_help="deletes an App Profile")
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def app_profile_delete(ctx, name, force):
    """deletes an App Profile

    NAME: name or ID of the App Profile to be deleted
    """
    logger.debug("")
    ctx.obj.app_profile.delete(name, force=force)


@click.command(name="app-profile-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@print_output.output_option
@click.pass_context
def app_profile_list(ctx, filter, output):
    """list all App Profiles"""
    logger.debug("")
    common.generic_list(callback=ctx.obj.app_profile.list, filter=filter, format=output)


@click.command(
    name="app-profile-show",
    short_help="shows the details of an App Profile",
)
@click.argument("name")
@print_output.output_option
@click.pass_context
def app_profile_show(ctx, name, output):
    """shows the details of an App Profile

    NAME: name or ID of the App Profile
    """
    logger.debug("")
    common.generic_show(callback=ctx.obj.app_profile.get, name=name, format=output)


@click.command(name="app-profile-update", short_help="updates an App Profile")
@click.argument("name")
@click.option("--newname", help="New name for the App Profile")
@click.option("--description", help="human readable description")
@click.pass_context
def app_profile_update(ctx, name, newname, description, **kwargs):
    """updates an App Profile

    NAME: name or ID of the App Profile
    """
    logger.debug("")
    profile_changes = common.generic_update(newname, description, kwargs)
    ctx.obj.app_profile.update(name, changes_dict=profile_changes)
