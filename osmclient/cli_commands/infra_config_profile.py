#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

import click
from osmclient.cli_commands import common
from osmclient.common import print_output
import logging

logger = logging.getLogger("osmclient")


@click.command(
    name="infra-config-profile-create", short_help="creates an Infra Config Profile"
)
@click.argument("name")
@click.option("--description", default="", help="human readable description")
@click.pass_context
def infra_config_profile_create(ctx, name, description, **kwargs):
    """creates an Infra Config Profile

    NAME: name of the Infra Config Profile
    """
    logger.debug("")
    kwargs = {k: v for k, v in kwargs.items() if v is not None}
    infra_config_profile = kwargs
    infra_config_profile["name"] = name
    if description:
        infra_config_profile["description"] = description
    ctx.obj.infra_config_profile.create(name, content_dict=infra_config_profile)


@click.command(
    name="infra-config-profile-delete", short_help="deletes an Infra Config Profile"
)
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def infra_config_profile_delete(ctx, name, force):
    """deletes an Infra Config Profile

    NAME: name or ID of the Infra Config Profile to be deleted
    """
    logger.debug("")
    ctx.obj.infra_config_profile.delete(name, force=force)


@click.command(name="infra-config-profile-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@print_output.output_option
@click.pass_context
def infra_config_profile_list(ctx, filter, output):
    """list all Infra Config Profiles"""
    logger.debug("")
    common.generic_list(
        callback=ctx.obj.infra_config_profile.list, filter=filter, format=output
    )


@click.command(
    name="infra-config-profile-show",
    short_help="shows the details of an Infra Config Profile",
)
@click.argument("name")
@print_output.output_option
@click.pass_context
def infra_config_profile_show(ctx, name, output):
    """shows the details of an Infra Config Profile

    NAME: name or ID of the Infra Config Profile
    """
    logger.debug("")
    common.generic_show(
        callback=ctx.obj.infra_config_profile.get, name=name, format=output
    )


@click.command(
    name="infra-config-profile-update", short_help="updates an Infra Config Profile"
)
@click.argument("name")
@click.option("--newname", help="New name for the Infra Config Profile")
@click.option("--description", help="human readable description")
@click.pass_context
def infra_config_profile_update(ctx, name, newname, description, **kwargs):
    """updates an Infra Config Profile

    NAME: name or ID of the Infra Config Profile
    """
    logger.debug("")
    profile_changes = common.generic_update(newname, description, kwargs)
    ctx.obj.infra_config_profile.update(name, changes_dict=profile_changes)
