#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

import click
import logging
from osmclient.common import print_output

logger = logging.getLogger("osmclient")


@click.command(name="profile-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@click.pass_context
def profile_list(ctx, filter):
    """list all Profiles"""
    if filter:
        filter = "&".join(filter)

    resp_infra_controller_profile = ctx.obj.infra_controller_profile.list(filter)
    resp_infra_config_profile = ctx.obj.infra_config_profile.list(filter)
    resp_app_profile = ctx.obj.app_profile.list(filter)
    resp_resource_profile = ctx.obj.resource_profile.list(filter)

    headers = ["Name", "Id", "Profile Type", "Default"]
    rows = []

    profile_list = [
        {"type": "Infra Controller Profile", "list": resp_infra_controller_profile},
        {"type": "Infra Config Profile", "list": resp_infra_config_profile},
        {"type": "App Profile", "list": resp_app_profile},
        {"type": "Resource Profile", "list": resp_resource_profile},
    ]
    for profile in profile_list:
        profile_type = profile["type"]
        profile_list_item = profile["list"]
        for item in profile_list_item:
            rows.append(
                [
                    item["name"],
                    item["_id"],
                    profile_type,
                    item["default"],
                ]
            )
    print_output.print_output("table", headers, rows)


def patch_cluster_profile(ctx, profile, cluster, profile_type, patch_string):
    logger.debug("")
    get_function = {
        "infra-controller-profile": ctx.obj.infra_controller_profile.get,
        "infra-config-profile": ctx.obj.infra_config_profile.get,
        "app-profile": ctx.obj.app_profile.get,
        "resource-profile": ctx.obj.resource_profile.get,
    }
    resp = get_function[profile_type](profile)
    profile_id = resp["_id"]
    resp = ctx.obj.cluster.get(cluster)
    cluster_id = resp["_id"]
    update_dict = {patch_string: [{"id": profile_id}]}
    logger.debug(
        f"Cluster_id: {cluster_id}. Profile_id: {profile_id}. Update_dict = {update_dict}"
    )
    return cluster_id, update_dict


@click.command(name="attach-profile")
@click.argument("profile")
@click.argument("cluster")
@click.option(
    "--profile-type",
    "--profile_type",
    type=click.Choice(
        [
            "infra-controller-profile",
            "infra-config-profile",
            "app-profile",
            "resource-profile",
        ]
    ),
    prompt=True,
    help="type of profile",
)
@click.pass_context
def attach_profile(ctx, profile, cluster, profile_type):
    """attaches profile to cluster

    \b
    PROFILE: name or ID of the profile
    CLUSTER: name or ID of the Kubernetes cluster
    """
    logger.debug("")
    cluster_id, update_dict = patch_cluster_profile(
        ctx, profile, cluster, profile_type, "add_profile"
    )
    ctx.obj.cluster.update_profiles(cluster_id, profile_type, update_dict)


@click.command(name="detach-profile")
@click.argument("profile")
@click.argument("cluster")
@click.option(
    "--profile-type",
    "--profile_type",
    type=click.Choice(
        [
            "infra-controller-profile",
            "infra-config-profile",
            "app-profile",
            "resource-profile",
        ]
    ),
    prompt=True,
    help="type of profile",
)
@click.pass_context
def detach_profile(ctx, profile, cluster, profile_type):
    """detaches profile from cluster

    \b
    PROFILE: name or ID of the profile
    CLUSTER: name or ID of the Kubernetes cluster
    """
    logger.debug("")
    cluster_id, update_dict = patch_cluster_profile(
        ctx, profile, cluster, profile_type, "remove_profile"
    )
    ctx.obj.cluster.update_profiles(cluster_id, profile_type, update_dict)
