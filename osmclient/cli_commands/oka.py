#######################################################################################
# Copyright ETSI Contributors and Others.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#######################################################################################

import click
from osmclient.cli_commands import common
from osmclient.common import print_output
import logging

logger = logging.getLogger("osmclient")


@click.command(
    name="oka-generate", short_help="Generate OKA folder structure and files"
)
@click.argument("name")
@click.option(
    "--base-directory",
    default=".",
    help=('Set the base location for OKA folder. Default: "."'),
)
@click.option(
    "--profile-type",
    type=click.Choice(
        [
            "infra-controller-profile",
            "infra-config-profile",
            "app-profile",
            "resource-profile",
        ]
    ),
    default="app-profile",
    help="type of profile. Default: app-profile",
)
@click.option(
    "--helm-repo-name",
    required=True,
    help="Helm repository name",
)
@click.option(
    "--helm-repo-url",
    required=True,
    help="Helm repository URL",
)
@click.option(
    "--helm-chart",
    required=True,
    help="Helm chart name (as present in the helm repo)",
)
@click.option(
    "--version",
    default="latest",
    help="Version of the helm chart",
)
@click.option(
    "--namespace",
    required=True,
    help="Default namespace where the OKA will be deployed",
)
@click.pass_context
def oka_generate(
    ctx,
    name,
    base_directory,
    profile_type,
    **kwargs,
    # helm_repo_name,
    # helm_repo_url,
    # helm_chart,
    # version,
    # namespace,
):
    """
    Creates an OKA folder structure with manifests and templates

    \b
    NAME: name of the OKA (matches the name of the destination folder)
    """
    logger.debug("")
    ctx.obj.oka.generate(
        name,
        base_directory,
        profile_type,
        kwargs,
    )


@click.command(name="oka-add", short_help="adds an OSM Kubernetes Application (OKA)")
@click.argument("name")
@click.argument("path")
@click.option(
    "--profile-type",
    type=click.Choice(
        [
            "infra-controller-profile",
            "infra-config-profile",
            "app-profile",
            "resource-profile",
        ]
    ),
    default="app-profile",
    help="type of profile. Default: app-profile",
)
@click.option("--description", default="", help="human readable description")
@click.pass_context
def oka_add(ctx, name, path, profile_type, description):
    """adds an OKA to OSM

    \b
    NAME: name of the OKA
    PATH: path to a folder with the OKA or to a tar.gz file with the OKA
    """
    logger.debug("")
    profile_type_mapping = {
        "infra-controller-profile": "infra_controller_profiles",
        "infra-config-profile": "infra_config_profiles",
        "app-profile": "app_profiles",
        "resource-profile": "resource_profiles",
    }
    oka = {}
    oka["name"] = name
    oka["profile_type"] = profile_type_mapping[profile_type]
    if description:
        oka["description"] = description
    ctx.obj.oka.create(name, content_dict=oka, filename=path)


@click.command(
    name="oka-delete", short_help="deletes an OSM Kubernetes Application (OKA)"
)
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def oka_delete(ctx, name, force):
    """deletes an OkA from OSM

    NAME: name or ID of the OKA to be deleted
    """
    logger.debug("")
    ctx.obj.oka.delete(name, force=force)


@click.command(name="oka-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@print_output.output_option
@click.pass_context
def oka_list(ctx, filter, output):
    """list all OSM Kubernetes Application (OKA)"""
    logger.debug("")
    common.generic_list(callback=ctx.obj.oka.list, filter=filter, format=output)


@click.command(
    name="oka-show",
    short_help="shows the details of an OKA",
)
@click.argument("name")
@print_output.output_option
@click.pass_context
def oka_show(ctx, name, output):
    """shows the details of an OKA

    NAME: name or ID of the OKA
    """
    logger.debug("")
    common.generic_show(callback=ctx.obj.oka.get, name=name, format=output)


@click.command(
    name="oka-update", short_help="updates an OSM Kubernetes Application (OKA)"
)
@click.argument("name")
@click.option("--newname", help="New name for the OSM Kubernetes Application (OKA)")
@click.option("--description", help="human readable description")
@click.pass_context
def oka_update(ctx, name, newname, description, **kwargs):
    """updates an OSM Kubernetes Application (OKA)

    NAME: name or ID of the OSM Kubernetes Application (OKA)
    """
    logger.debug("")
    oka_changes = common.generic_update(newname, description, kwargs)
    ctx.obj.oka.update(name, changes_dict=oka_changes, force_multipart=True)


@click.command(
    name="oka-update-content",
    short_help="updates the content of an OKA",
)
@click.argument("name")
@click.argument("path")
@click.pass_context
def oka_update_content(ctx, name, path):
    """updates the content of an OSM Kubernetes Application (OKA)

    \b
    NAME: name or ID of the OSM Kubernetes Application (OKA)
    PATH: path to a folder with the OKA or to a tar.gz file with the OKA
    """
    logger.debug("")
    ctx.obj.oka.update_content(name, path)
