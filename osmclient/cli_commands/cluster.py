# Copyright ETSI Contributors and Others.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import click
from osmclient.cli_commands import common
from osmclient.common import print_output
import logging
import yaml

logger = logging.getLogger("osmclient")


@click.command(name="cluster-create", short_help="creates a K8s cluster")
@click.argument("name")
@click.option("--node-count", "-n", prompt=True, type=int, help="number of nodes")
@click.option("--node-size", prompt=True, help="size of the worker nodes")
@click.option("--version", prompt=True, help="Kubernetes version")
@click.option(
    "--vim-account",
    prompt=True,
    help="VIM target or cloud account where the cluster will be created",
)
@click.option("--description", default="", help="human readable description")
@click.option(
    "--region-name",
    default=None,
    help="region name in VIM account where the cluster will be created",
)
@click.option(
    "--resource-group",
    default=None,
    help="resource group in VIM account where the cluster will be created",
)
@click.option(
    "--bootstrap/--no-bootstrap",
    required=False,
    default=True,
    help="bootstrap the cluster with Flux (prepare for GitOps) (default=True)",
)
@click.pass_context
def cluster_create(
    ctx,
    name,
    node_count,
    node_size,
    version,
    vim_account,
    description,
    region_name,
    resource_group,
    bootstrap,
    **kwargs
):
    """creates a K8s cluster

    NAME: name of the K8s cluster
    """
    logger.debug("")
    # kwargs = {k: v for k, v in kwargs.items() if v is not None}
    cluster = {}
    cluster["name"] = name
    cluster["node_count"] = node_count
    cluster["node_size"] = node_size
    cluster["k8s_version"] = version
    cluster["vim_account"] = vim_account
    if description:
        cluster["description"] = description
    if region_name:
        cluster["region_name"] = region_name
    if resource_group:
        cluster["resource_group"] = resource_group
    cluster["bootstrap"] = bootstrap
    ctx.obj.cluster.create(name, content_dict=cluster)


@click.command(name="cluster-delete", short_help="deletes a K8s cluster")
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def cluster_delete(ctx, name, force):
    """deletes a K8s cluster

    NAME: name or ID of the K8s cluster to be deleted
    """
    logger.debug("")
    ctx.obj.cluster.delete(name, force=force)


@click.command(name="cluster-list")
@click.option(
    "--filter",
    help="restricts the list to the items matching the filter",
)
@print_output.output_option
@click.pass_context
def cluster_list(ctx, filter, output):
    """list all K8s clusters"""
    logger.debug("")
    extra_items = [
        {
            "header": "Created by OSM",
            "item": "created",
        },
        {
            "header": "Version",
            "item": "k8s_version",
        },
        {
            "header": "VIM account",
            "item": "vim_account",
        },
        {
            "header": "Bootstrap",
            "item": "bootstrap",
        },
        {
            "header": "Description",
            "item": "description",
        },
    ]
    common.generic_list(
        callback=ctx.obj.cluster.enhanced_list,
        filter=filter,
        format=output,
        extras=extra_items,
    )


@click.command(
    name="cluster-show",
    short_help="shows the details of a K8s cluster",
)
@click.argument("name")
@print_output.output_option
@click.pass_context
def cluster_show(ctx, name, output):
    """shows the details of a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    common.generic_show(callback=ctx.obj.cluster.get, name=name, format=output)


@click.command(
    name="cluster-edit", short_help="updates name or description of a K8s cluster"
)
@click.argument("name")
@click.option("--newname", help="New name for the K8s cluster")
@click.option("--description", help="human readable description")
@click.pass_context
def cluster_edit(ctx, name, newname, description, **kwargs):
    """updates the name or description of a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    cluster_changes = common.generic_update(newname, description, kwargs)
    ctx.obj.cluster.update(name, changes_dict=cluster_changes)


@click.command(
    name="cluster-upgrade", short_help="changes the version of a K8s cluster"
)
@click.argument("name")
@click.option("--version", prompt=True, help="Kubernetes version")
@click.pass_context
def cluster_upgrade(ctx, name, version, **kwargs):
    """changes the version of a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    cluster_changes = {
        "k8s_version": version,
    }
    ctx.obj.cluster.upgrade(name, cluster_changes)


@click.command(name="cluster-scale", short_help="scales a K8s cluster")
@click.argument("name")
@click.option("--node-count", "-n", prompt=True, type=int, help="number of nodes")
@click.pass_context
def cluster_scale(ctx, name, node_count, **kwargs):
    """scales the number of nodes of a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    cluster_changes = {
        "node_count": node_count,
    }
    ctx.obj.cluster.scale(name, cluster_changes)


@click.command(name="cluster-update", short_help="updates a K8s cluster")
@click.argument("name")
@click.option("--version", help="Kubernetes version")
@click.option("--node-count", "-n", type=int, help="number of nodes")
@click.option("--node-size", help="size of the worker nodes")
@click.pass_context
def cluster_update(ctx, name, version, node_count, node_size, **kwargs):
    """updates the number of nodes, the version or the node size of a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    cluster_changes = {}
    if version:
        cluster_changes["k8s_version"] = version
    if node_count:
        cluster_changes["node_count"] = node_count
    if node_size:
        cluster_changes["node_size"] = node_size
    ctx.obj.cluster.fullupdate(name, cluster_changes)


@click.command(
    name="cluster-get-credentials", short_help="gets kubeconfig of a K8s cluster"
)
@click.argument("name")
@click.pass_context
def cluster_get_credentials(ctx, name, **kwargs):
    """gets the kubeconfig file to access a K8s cluster

    NAME: name or ID of the K8s cluster
    """
    logger.debug("")
    ctx.obj.cluster.get_credentials(name)


# Alias for the cluster-get-credentials command
@click.command(
    name="cluster-get-kubeconfig", short_help="gets kubeconfig of a K8s cluster"
)
@click.argument("name")
@click.pass_context
def cluster_get_kubeconfig(ctx, name, **kwargs):
    """Alias for the cluster-get-credentials command"""
    ctx.invoke(cluster_get_credentials, name=name)


@click.command(name="cluster-register", short_help="registers a K8s cluster to OSM")
@click.argument("name")
@click.option(
    "--vim-account",
    "--vim",
    prompt=True,
    help="VIM target or cloud account where the cluster will be created",
)
@click.option(
    "--creds", prompt=True, help="credentials file, i.e. a valid `.kube/config` file"
)
@click.option("--description", default="", help="human readable description")
@click.option(
    "--bootstrap/--no-bootstrap",
    required=False,
    default=True,
    help="bootstrap the cluster with Flux (prepare for GitOps) (default=True)",
)
@click.pass_context
def cluster_register(
    ctx,
    name,
    vim_account,
    creds,
    description,
    bootstrap,
):
    """registers a K8s cluster to OSM

    NAME: name of the K8s cluster
    """
    logger.debug("")
    cluster = {}
    cluster["name"] = name
    with open(creds, "r") as cf:
        cluster["credentials"] = yaml.safe_load(cf.read())
    cluster["vim_account"] = vim_account
    if description:
        cluster["description"] = description
    cluster["bootstrap"] = bootstrap
    ctx.obj.cluster.register(name, cluster)


@click.command(name="cluster-deregister", short_help="deregisters a K8s cluster")
@click.argument("name")
@click.option(
    "--force", is_flag=True, help="forces the deletion from the DB (not recommended)"
)
@click.pass_context
def cluster_deregister(ctx, name, force):
    """deregisters a K8s cluster

    NAME: name or ID of the K8s cluster to be deregistered
    """
    logger.debug("")
    ctx.obj.cluster.deregister(name, force=force)
